---
title: Projet Mécanique 2021
author: MÉLOIS Youn, ROBIN Guillaume
---

# Projet Mécanique 2021

## Sommaire

- [Introduction](#introduction)
- [Partie I - Observer](#partie-i-observer)
    - [Trajectoire du mouvement](#trajectoire-du-mouvement)
    - [Evolution temporelle du mouvement](#evolution-temporelle-du-mouvement)
    - [Analyse qualitative](#analyse-qualitative)
- [Partie II - Modéliser](#partie-ii-mod%C3%A9liser)
    - [Présentation de la situation physique](#pr%C3%A9sentation-de-la-situation-physique)
    - [Modélisation théorique : Application des lois de Newton pour obtenir les équations horaires théoriques](#mod%C3%A9lisation-th%C3%A9orique-application-des-lois-de-newton-pour-obtenir-les-%C3%A9quations-horaires-th%C3%A9oriques)
    - [Étude énergétique du mouvement](#%C3%A9tude-%C3%A9nerg%C3%A9tique-du-mouvement)
    - [Vérification des modèles](#v%C3%A9rification-des-mod%C3%A8les)
    - [Amélioration du modèles - Pour approfondir](#am%C3%A9lioration-du-mod%C3%A8les-pour-approfondir)
- [Partie III - Prévoir](#partie-iii-pr%C3%A9voir)
    - [Choisir une nouvelle situation expérimentale](#choisir-une-nouvelle-situation-exp%C3%A9rimentale)
    - [Calculer la trajectoire grâce au modèle mathématique](#calculer-la-trajectoire-gr%C3%A2ce-au-mod%C3%A8le-math%C3%A9matique)
    - [Réaliser une nouvelle expérience](##r%C3%A9aliser-une-nouvelle-exp%C3%A9rience)
    - [Confronter la prévision réalisée grâce au modèle, et la réalité de l'expérience](#confronter-la-pr%C3%A9vision-r%C3%A9alis%C3%A9e-gr%C3%A2ce-au-mod%C3%A8le-et-la-r%C3%A9alit%C3%A9-de-lexp%C3%A9rience)
- [Partie IV - Conclure](#partie-iv-conclure)
- [Partie V - Analyse reflexive](#partie-v-analyse-reflexive)

## Introduction

L'objectif de ce projet est d'étudier un mouvement physique que nous avons au préalable choisit afin d'en comprendre avec précision les cause et d'en déduire une prédiction.

Nous avons choisi pour ce projet le mouvement d'une bille de métal projetée par la chute d'un marteau attaché à un axe. Nous étudions en conséquence le système de la bille.

Nous prenons, dans l'hypothèse initiale, en compte les frottements et la poussée d'Archimède. Nous pouvons émettre deux hypotheses au préalable, les forces de frottements ne nous semblent pas négligeable contrairement a la poussée archimede qui le serait probablement puisque le fluide dans lequel ne systeme évolue est l'air.

Dans l'étude de ce systeme, nous allons nous appuyer sur un modèle théorique basé sur les lois de Newton, ainsi que d'un modele experimental donc obtenue a partir d'une modélisation mathématique qui est le pointage du mouvement effectué sur le logiciel Latis Pro ainsi que regressi. Ensuite, nous pourrons en déduire une approximation de notre modele par rapport a la réalité.

Notre projet, sera donc decomposer en partie elle même rassemblée en trois grandes étapes de réflexion qui sont : l'observation, la modélisation ainsi que la prévision.

Enfin ce mouvement nous a semblé pertinent à étudier, reliant tout les chapitres de mécanique vus précedement.


## Partie I - Observer

### Trajectoire du mouvement

Le mouvement étudié est celui d'une bille projetée par la chutte d'un marteau. Le système étudié est donc celui de la bille.
Nous nous plaçons dans un référentiel *terrestre supposé galiléen* où le repère est cartésien orthonormé ($`O`$; $`\vec{e_x}`$; $`\vec{e_y}`$).

À l'aide de ce [pointage](./data-plot.md) vidéo effectué à l'aide du logiciel [Regressi](http://regressi.fr/WordPress/) nous obtenons la trajectoire du mouvement ci-dessous : 

![trajectoire][trajectoire]

Les vecteurs vitesse en rouge sont tout à fait cohérents du fait que leur composante est tengente à la trajectoire à l'origine. Le vecteur accéleration à l'air de correspondre à $`g`$, l'accéleration gravitationnelle terrestre, on en déduit donc que lui aussi est cohérent avec la trajectoire.

### Evolution temporelle du mouvement

![evolution temporelle mouvement][evolution-temp-mouvement]

![evolution temp vitesse+acceleration][evolution-temp-vitesse+acceleration]

### Analyse qualitative

La trajectoire décrit une parabole, mouvement cohérent dans le domaine réel, les composantes ce celle ci sont tout aussi cohérentes : le mouvement en $`x`$ est linéaire et le mouvement en $`y`$ décrit une parabole, ce qui correspond parfaitement à un mouvement sans frottements.

En revanche, le calcul de la vitesse et de l'accéleration n'a pas l'air correct. Or il existe l'option "Modélisation" dans Regressi, grâce à cet outil nous pouvons modéliser une équations qui passe à peu près par les points du pointage. Nous obtenons alors ce graphique :

![evolution temp vitesse+acceleration modele][evolution-temp-vitesse+acceleration-modele]

Ce dernier modèle semble plus cohérent que le précédent, l'accéleration est constante et la vitesse correspond bien à un mouvement parabolique.

[trajectoire]: ./images/trajectoire-vecteurs.png
[evolution-temp-mouvement]: ./images/evolution-temp-mouvement.png
[evolution-temp-vitesse+acceleration]: ./images/evolution-temp-vitesse+acceleration.png
[evolution-temp-vitesse+acceleration-modele]: ./images/evolution-temp-vitesse+acceleration-modele.png


## Partie II - Modéliser

### Présentation de la situation physique
Le référentiel choisit est un référentiel terrestre supposé galiléen, l'origine du repere étant le centre de la Terre et ses axes suivent sa rotation. De plus, bien que le réferentiel terrestre ne soit pas fondamentallement galiléen de par la rotation de la Terre, il est ici bien considéré comme tel du a la durée du mouvment relativement courte et vérifie donc bien le principe d'inertie.

Le système étudié est une bille, de masse $`m = 32 \pm 1 \space g`$ pour un diamètre $`d = 20 \space mm \space = 2 \cdot 10^{-2} \space m`$. Nous en déduisons donc sa masse volumique $`\rho = 7639 \pm 239 \space kg.m^{-3}`$ et nous pouvons, en conséquent, déterminer le matériau de la bille qui serait probablement faite de fer ($`\rho_{fer} = 7870 \space kg.m^{-3}`$) grâce à ce [tableau des densités](https://en.wikipedia.org/wiki/Density#Densities). Nous pourrions alors poser une masse théorique $`m_{théorique} = \rho  \cdot  V = \rho  \cdot  \frac{4}{3} \pi r^3 \approx 32.96 \space g`$. Ce système est également mis en mouvement par le contact d'un marteau à un angle $`\alpha = 43 \space °`$. On prendra $`g=9.8 \space m.s^{-2}`$, $`\rho_{air}=1.2 \space kg.m^{-3}`$. 

On étudie la chute libre de la bille, on peut donc affirmer que seuls 3 forces s'appliquent sur le systeme : 

- Le poids : $`\vec{P} = m \vec{g}`$ avec $`\lVert\vec{P}\rVert = m_{théorique}  \cdot  g \approx 0.323 \space N`$
- La poussée d'Archimède : $`\vec{\Pi_A} = \rho_{déplacé}  \cdot  V_{déplacé}  \cdot  \vec{g}`$ avec $`\lVert\vec{\Pi_A}\rVert = \rho_{air}  \cdot  \frac{4}{3} \pi r^3  \cdot  g \approx 49.2602 \space \mu N`$
- La force de frottements : $`\vec{f_f}= -\lambda \vec{v}`$

Nous pouvons négliger la poussée d'Archimède, celle ci étant infime par rapport au poids. $`\lVert\vec{P}\rVert\gg\lVert\vec{\Pi_A}\rVert`$

Pour trouver $`\lambda`$ on utilise la [loi de Stokes](https://en.wikipedia.org/wiki/Stokes%27_law), qui stipule : $`f_f= 6 \pi \mu r v`$ avec $`\mu`$ la viscosité dynamique du fluide ($`Pa.s`$), et $`r`$ le rayon de la sphere. Donc $`\lambda = 6 \pi \mu r`$. On [trouve](https://en.wikipedia.org/wiki/Viscosity#Air) $`\mu_{air} = 2.791  \cdot  10^{-7}  \cdot  T^{0.7355}`$ avec $`T`$ la température en Kelvin, on a alors pour $`T=22 \space ° C = 295.15 \space ° K`$, $`\mu_{air} \approx 18.3  \cdot  10^{-6} \space Pa.s`$. On en déduit $`\lambda = 6 \pi  \cdot  18.3  \cdot  10^{-6}  \cdot  1  \cdot  10^{-2} \approx 3.45  \cdot  10^{-6} \space kg.s^{-1}`$.

Pour une vitesse de l'ordre de $`10^1`$, l'ordre de la force de frottements est alors de $`10^{-5}`$, nous pouvons négliger la force de frottement car $`\lVert\vec{P}\rVert\gg\lVert\vec{f_f}\rVert`$.

![schéma de la situation][schema-situation]

Conditions initiales à $`t = 0`$: 

$`x(0) = 0`$

$`y(0) = y_0 = 0.3206`$

$`\dot{x}(0) = {v_0}\cos \alpha`$

$`\dot{y}(0) = {v_0}\sin \alpha`$

### Modélisation théorique : Application des lois de Newton pour obtenir les équations horaires théoriques

D'après la seconde loi de Newton, le principe fondamental de la dynamique : 

```math
\begin{aligned}
    \sum\vec{F_{ext}}=\frac{d\vec{p}}{dt}
\end{aligned}
```

La masse étant constante durant l'intégralité du mouvement on a :

```math
\begin{aligned}
    \sum\vec{F_{ext}}&= m\vec{a}\\
    \vec{P} + \vec{f_f} &= m\vec{a}\\
    m \vec{g} &= m\vec{a}\\
    -g\vec{e_y} &= \vec{a_x} + \vec{a_y}\\
    -g\vec{e_y} &= \ddot{x}\vec{e_x} + \ddot{y}\vec{e_y}\\
\end{aligned}
```

On obtient alors les composantes sur $`x`$ et $`y`$ suivantes :

```math
\begin{cases}
    \space \vec{e_x}/ \space \ddot{x} = 0 \\
    \space \vec{e_y}/ \space \ddot{y} = -g
\end{cases}
```

```math
\Downarrow
```

```math
\begin{aligned}
    \dot{x}(t) &= A\\
    \dot{y}(t) &= - g t + B
\end{aligned}
```

On trouve les constante $`A`$ et $`B`$ grâce aux conditions initiales :

```math
\begin{aligned}
    &\begin{cases}
        \dot{x}(0) = v_0 \cos \alpha\\
        \dot{x}(0) = A
    \end{cases}\\
    &\begin{cases}
        \dot{y}(0) = v_0 \sin \alpha\\
        \dot{y}(0) = B
    \end{cases}
\end{aligned}
```

```math
\begin{aligned}
    &\implies A = v_0 \cos (\alpha)\\
    &\implies B = v_0 \sin (\alpha)
\end{aligned}
```

```math
\begin{aligned}
    \dot{x}(t) &= v_0 \cos (\alpha)\\
    \dot{y}(t) &= - g t + v_0 \sin (\alpha)
\end{aligned}
```

Puis en intégrant :

```math
\begin{aligned}
    x &= v_0 \cos (\alpha) t + A'\\
    y &= - \frac{g}{2} t^2 + v_0 \sin (\alpha) t + B'
\end{aligned}
```

On trouve les constante $`A'`$ et $`B'`$ grâce aux conditions initiales :

```math
\begin{aligned}
    &\begin{cases}
        x(0) = 0\\
        x(0) = A'
    \end{cases}\\
    &\begin{cases}
        y(0) = 0.3206\\
        y(0) = B'
    \end{cases}
\end{aligned}
```

```math
\begin{aligned}
    &&\implies A' = 0\\
    &&\implies B' = 0.3206
\end{aligned}
```

```math
\begin{aligned}
    x &= v_0 \cos (\alpha) t\\
    y &= - \frac{g}{2} t^2 + v_0 \sin (\alpha) t + 0.3206
\end{aligned}
```

De plus, nous pouvons trouver $`v_0`$ à l'aide la conservation de l'énergie mécanique :

Dans un premier temps calculons $`v_{m_i}`$ la vitesse du marteau avant contact avec la bille.

```math
\begin{aligned}
    E_{c_i} + E_{p_i} = E_{c_f} + E_{p_f}
\end{aligned}
```

Or nous lachons le marteau sans impulsion initiale donc $`E_{c_i} = 0`$.

```math
\begin{aligned}
    E_{p_i} &= E_{c_f} + E_{p_f}\\
    mgH &= \frac{1}{2} m v_{m_i}^2 + mgh\\
    gH &= \frac{1}{2} v_{m_i}^2 + gh\\
    v_{m_i} &= \sqrt{2g(H-h)}
\end{aligned}
```

Où $`h`$ la hauteur de la bille, on sait que $`h = H(1-\cos(\alpha))`$.

```math
\begin{aligned}
        v_{m_i} &= \sqrt{2g(H-H(1-\cos(\alpha)))}\\
        v_{m_i} &= \sqrt{2gH(1-(1-\cos(\alpha)))}\\
        v_{m_i} &= \sqrt{2gH\cos(\alpha)}\\
\end{aligned}
```

Dans un second temps calculons $`v_{m_f}`$ la vitesse du marteau après contact avec la bille.

```math
\begin{aligned}
    E_{c_i} + E_{p_i} = E_{c_f} + E_{p_f}
\end{aligned}
```

Or le marteau atteindra son maximum d'où $`E_{c_f} = 0`$.

```math
\begin{aligned}
        E_{c_i} + E_{p_i} &= E_{p_f}\\
        \frac{1}{2} m v_{m_f}^2 + m g h &= m g h_f\\
        v_{m_f} &= \sqrt{2g(h_f - h)}\\
        v_{m_f} &= \sqrt{2g(h_f - H(1-\cos(\alpha)))}\\
\end{aligned}
```

Où $`h_f`$ la hauteur finale qu'atteint le marteau.

Par expérimentation, nous pouvons trouver une relation entre $`h_f`$ et $`H`$. Nous le faisons par pointage vidéo.

| $`H`$                | $`h_f`$                 |
| :------------------- | :---------------------- |
| $`55 \cdot 10^{-2}`$ | $`50.2 \cdot 10^{-2}`$  |
| $`60 \cdot 10^{-2}`$ | $`55.27 \cdot 10^{-2}`$ |

Nous pouvons en déduire une droite linéaire moyenne des points ci-dessus par la méthode suivante :

Pour un nombre $`N`$ de points de coordonnées $`(x_k, y_k)`$ on a la droite linéaire moyenne suivante :

```math
\begin{aligned}
    y(x) = \frac{\displaystyle\sum_{k=1}^N \frac{y_k}{x_k}}{N} \cdot x
\end{aligned}
```

Où $`\frac{\sum_{k=1}^N \frac{y_k}{x_k}}{N}`$ la pente moyenne.

Par application numérique nous obtenons :

```math
\begin{aligned}
    h_f = \left(\frac{\frac{50.4}{55} + \frac{55.25}{60}}{2} \approx 0.917 \right) \cdot H
\end{aligned}
```

On a alors :

```math
\begin{aligned}
        v_{m_f} &= \sqrt{2g(0.917 \cdot H - H(1-\cos(\alpha)))}\\
        v_{m_f} &= \sqrt{2gH(0.917 - 1 +\cos(\alpha))}\\
        v_{m_f} &= \sqrt{2gH(\cos(\alpha) - 0.083)}\\
\end{aligned}
```

Finalement par conservation du mouvement nous obtenons :

```math
\begin{aligned}
        m_m v_{m_i} &= m_m v_{m_f} + m_b v_0\\
        v_0 &= \frac{m_m}{m_b} \cdot (v_{m_i} - v_{m_f})\\
        v_0 &= \frac{m_m}{m_b} \cdot \left( \sqrt{2gH\cos(\alpha)} - \sqrt{2gH(\cos(\alpha) - 0.083)} \right)\\
        v_0 &= \frac{m_m}{m_b} \cdot \sqrt{2gH} \cdot \left( \sqrt{\cos(\alpha)} - \sqrt{\cos(\alpha) - 0.083} \right)\\
\end{aligned}
```

### Étude énergétique du mouvement

Nous pouvons identifier trois forces travaillant sur le systeme, le poids et la poussée archimede qui sont des forces conservatives et les frottements qui est une force non conservative.

On prend $`B`$ pour position initiale de la bille et $`M`$ sa position lors du mouvement.

On a dans notre système 3 forces, le poids $`\vec{P}`$, la poussée d'Archimède $`\vec{\Pi_A}`$, la force de frottement $`\vec{f_f}`$. On repère 2 phases dans le mouvement, ascendant et descendant. On en déduit donc le travail des différentes forces lors de l'ascention : $`\vec{P}`$ résistant, $`\vec{\Pi_A}`$ moteur et $`\vec{f_f}`$ résistant; et lors de la descente: $`\vec{P}`$ moteur, $`\vec{\Pi_A}`$ résistant et $`\vec{f_f}`$ résistant.

On applique le théorème de l'énergie cinétique (TEC) entre les points B et M.

```math
\begin{aligned}
    \Delta_{B \to M} E_c = E_{c_M} - E_{c_B} = W_{B \to M}(\vec{P})
\end{aligned}
```

La bille à une vitesse initiale de $`v_0`$ donc $`E_{c_B} = \frac{1}{2} m v_0^2`$. Elle atteint le point M à la vitesse $`\vec{v}`$ de norme $`v`$. La vatiation d'énergie cinétique s'écrit donc :

```math
\begin{aligned}
    \Delta_{B \to M} E_c = E_{c_M} - E_{c_B} = \frac{1}{2} m v^2 - \frac{1}{2} m v_0^2 = \frac{1}{2} m (v^2 - v_0^2)
\end{aligned}
```

Nous négligeons la poussée d'Archimède et la force de frottement. On a donc:

```math
\begin{aligned}
    \delta W (\vec{P}) = - m g \vec{e_y} . d \overrightarrow{OM} = - m g \space dy
\end{aligned}
```

Puis :

```math
\begin{aligned}
        W_{B \to M} (\vec{P}) &= \int^{y_M}_{y_B} \delta W \space dy\\ 
        W_{B \to M} (\vec{P}) &= - m g [ y ]^{y_M}_{y_B}\\ 
        W_{B \to M} (\vec{P}) &= - m g (y_M - y_B)\\
        W_{B \to M} (\vec{P}) &= m g (y_B - y_M)\\
        W_{B \to M} (\vec{P}) &= m g (y_0 - y)
\end{aligned}
```

Le travail du poids est négatif, il s'agit donc bien d'un travail résistant. On obtient donc finalement :

```math
\begin{aligned}
        \Delta_{B \to M} E_c &= W_{B \to M} (\vec{P})\\
        \frac{1}{2} m (v^2 - v_0^2) &= m g (y_0 - y)\\
        v^2 - v_0^2 &= 2 g (y_0 - y)\\
        v &= \sqrt{2 g (y_0 - y) + v_0^2}
\end{aligned}
```

Par application numérique, pour $`y = y_0`$ :

$`v = \lvert v_0 \rvert = v_0 \space m.s^{-1}`$ (c'est cohérent).

On trouve alors les différentes énergies comme suit :

$`E_c = \frac{1}{2} m v^2`$

$`E_{pp} = m g y`$

$`E_m = E_c + E_{pp}`$

Par application numérique on trouve :

```math
\begin{aligned}
        E_c &= \frac{1}{2} m v^2\\
        E_c &= \frac{1}{2} m \sqrt{2 g (y_0 - y) + v_0^2}^2\\
        E_c &= \frac{1}{2} m \lvert 2 g (y_0 - y) + v_0^2 \rvert\\
        E_c &= m \lvert g (y_0 - y) + \frac{v_0^2}{2} \rvert\\
        E_c &= \begin{cases}
            m ( g (y_0 - y) + \frac{v_0^2}{2}) & si \space g (y_0 - y) + \frac{v_0^2}{2} \ge 0\\
            - m ( g (y_0 - y) + \frac{v_0^2}{2}) & sinon
        \end{cases}
\end{aligned}
```

Cherchons quand $`g (y_0 - y) + \frac{v_0^2}{2} \ge 0`$ :

```math
\begin{aligned}
        g (y_0 - y) + \frac{v_0^2}{2} &\ge 0\\
        g \cdot y_0 - g \cdot y + \frac{v_0^2}{2} &\ge 0\\
        g \cdot y_0 + \frac{v_0^2}{2} &\ge g \cdot y\\
        y_0 + \frac{v_0^2}{2 g} &\ge y\\
\end{aligned}
```

On obtient $`y_{max} = y(\frac{-b}{2a}) = y(\frac{- v_0 \sin (\alpha)}{2 \cdot \frac{-g}{2}}) = y(\frac{v_0 \sin (\alpha)}{g}) = \frac{v_0^2 \sin^2(\alpha)}{2g}+y_0`$.

Si $`y_0 + \frac{v_0^2}{2 g} \ge y_{max}`$ alors $`y_0 + \frac{v_0^2}{2 g} \ge y`$ :

```math
\begin{aligned}
        y_0 + \frac{v_0^2}{2 g} &\ge y_{max}\\
        y_0 + \frac{v_0^2}{2 g} &\ge \frac{v_0^2 \sin^2(\alpha)}{2g}+y_0\\
        \frac{v_0^2}{2 g} &\ge \frac{v_0^2 \sin^2(\alpha)}{2g}\\
        v_0^2 &\ge v_0^2 \sin^2(\alpha)\\
        1 &\ge \sin^2(\alpha)\\
\end{aligned}
```

Or $`\sin^2(t) \le 1`$ donc $`y_0 + \frac{v_0^2}{2 g} \ge y`$

On a donc pour $`E_c`$ :

```math
\begin{aligned}
        E_c &= m ( g (y_0 - y) + \frac{v_0^2}{2})
\end{aligned}
```

On calcule $`E_m`$ :

```math
\begin{aligned}
    E_m &= E_c + E_{pp}\\
    E_m &= m ( g (y_0 - y) + \frac{v_0^2}{2}) + m g y\\
    E_m &= m g ( y_0 - y + \frac{v_0^2}{2g}) + m g y\\
    E_m &= y_0 - y + \frac{v_0^2}{2g} + y\\
    E_m &= y_0 + \frac{v_0^2}{2g}\\
\end{aligned}
```

$`E_m`$ constante, on a conservation de l'énergie mécanique.

![tracé des courbes d'énergies](./images/trace-energetique.png)

[Ici le graphe Desmos](https://www.desmos.com/calculator/7gnrgn9ydp)

Prenons dorénavant en compte les forces de frottements.

On cherche a déterminer le travail des forces de frottements :

```math
\begin{aligned}
    \delta W (\vec{f_f}) = \vec{f_f} . d \overrightarrow{OM}
\end{aligned}
```

Puis :

```math
\begin{aligned}
        W_{B \to M} (\vec{f_f}) &= \int^{M}_{B} \delta W (\vec{f_f}) \space dl\\
        W_{B \to M} (\vec{f_f}) &= - \lambda v^2 \cdot d\\
\end{aligned}
```

Avec $`d`$ la distance entre $`B`$ et $`M`$ :

<!-- On trouve l'équation linéaire de la trajectoire $`y(x) = - \frac{g}{2 \cdot v_0^2 \cos^2(\alpha)}x^2 + \tan(\alpha)x + y_0`$. -->

On trouve $`d`$, la distance entre 2 points sur une parabole, par [intégration](https://math.stackexchange.com/a/389179) :

<!-- ```math
\begin{aligned}
    
        d &= \int^{x_M}_{x_B} \sqrt{1+(\frac{dy}{dx})^2} \space dx\\
        d &= \int^{x}_{0} \sqrt{1+(-\frac{g}{v_0^2 cos^2(\alpha)}x + \tan(\alpha))^2} \space dx\\
    
\end{aligned}
``` -->
```math
\begin{aligned}
        d &= \int^{t_M}_{t_B} \sqrt{1+(\frac{dy}{dt})^2} \space dt\\
        d &= \int^{t}_{0} \sqrt{1+(-gt + v_0 \sin(\alpha)} \space dt\\
\end{aligned}
```

<!-- On pose $`a = -\frac{g}{v_0^2 cos^2(\alpha)}`$ et $`b = \tan(\alpha)`$ : -->
On pose $`a = -g`$ et $`b = v_0 \sin(\alpha)`$ :

On trouve avec l'aide de [WolframAlpha](https://www.wolframalpha.com/input/?i=integral+of+sqrt%281%2B%28ax%2Bb%29%5E2%29+dx) une primitive.

<!-- ```math
\begin{aligned}
    
        d &= \int^{x}_{0} \sqrt{1+(ax + b)^2} \space dx\\
        d &= \begin{bmatrix}\frac{\sqrt{(ax+b)^2+1} \cdot (ax+b)+\sinh^{-1}(ax+b)}{2a}\end{bmatrix}^{x}_{0}\\
        d &= \frac{\sqrt{(ax+b)^2+1} \cdot (ax+b)+\sinh^{-1}(ax+b)}{2a} - \frac{\sqrt{b^2+1} \cdot b+\sinh^{-1}(b)}{2a}\\
        d &= \frac{\sqrt{(ax+b)^2+1} \cdot (ax+b)+\sinh^{-1}(ax+b) - \sqrt{b^2+1} \cdot b - \sinh^{-1}(b)}{2a}\\
    
\end{aligned}
``` -->
```math
\begin{aligned}
        d &= \int^{t}_{0} \sqrt{1+(at + b)^2} \space dt\\
        d &= \begin{bmatrix}\frac{\sqrt{(at+b)^2+1} \cdot (at+b)+\sinh^{-1}(at+b)}{2a}\end{bmatrix}^{t}_{0}\\
        d &= \frac{\sqrt{(at+b)^2+1} \cdot (at+b)+\sinh^{-1}(at+b)}{2a} - \frac{\sqrt{b^2+1} \cdot b+\sinh^{-1}(b)}{2a}\\
        d &= \frac{\sqrt{(at+b)^2+1} \cdot (at+b)+\sinh^{-1}(at+b) - \sqrt{b^2+1} \cdot b - \sinh^{-1}(b)}{2a}\\
\end{aligned}
```

En appliquant le TEC:

```math
\begin{aligned}
        \Delta_{B \to M} E_c = E_{c_M} - E_{c_B} &= W_{B \to M}(\vec{P}) + W_{B \to M}(\vec{f_f})\\
        \frac{1}{2} m (v^2 - v_0^2) &= m g (y_0 - y) - \lambda v^2 \cdot d\\
        \frac{1}{2} m v^2 - \frac{1}{2} m v_0^2 &= m g (y_0 - y) - \lambda v^2 \cdot d\\
        m v^2 + 2 \lambda v^2 \cdot d &= 2 m g (y_0 - y) + m v_0^2\\
        v^2 \cdot (m + 2 \lambda d) &= m \cdot (2 g (y_0 - y) + v_0^2)\\
        v^2 &= m \cdot \frac{2 g (y_0 - y) + v_0^2}{m + 2 \lambda d}\\
        v &= \sqrt{m \cdot \frac{2 g (y_0 - y) + v_0^2}{m + 2 \lambda d}}\\
\end{aligned}
```

A l'aide de Desmos on peut [grapher les courbes d'énergie](https://www.desmos.com/calculator/fqnflrl0c3).

Si l'on observe attentivement le modèle on peut se rendre compte que la courbe de l'énergie mécanique diminue très subtilement au cours du temps ce qui correspond bien à ce que l'on attendait en rajoutant la force de frottements.

### Vérification des modèles

Vérifions maintenant la cohérence de notre modele théorique par rapport au mouvement pointé. 

A l'aide de Latis pro on obtient :

![modélisation du mouvement](./images/modélisation_du_mouvement_pointé.PNG)

A l'aide de Desmos on peut comparer par la suite [l'équation de la trajectoire théorique et celle du mouvement pointé](https://www.desmos.com/calculator/gibke2cvki). 

![eq_trajectoire_theo_et_mouv_pointe](./images/eq_trajectoire_theo_et_mouv_pointe.PNG)

On constate donc que l'allure de la modélisation théorique et réel sont differentes de par l'effet des forces que nous avons négligé précedement comme les frottements et la poussée d'archimede.



### Amélioration du modèles - Pour approfondir

D'après la seconde loi de Newton, le principe fondamental de la dynamique : 

```math
\begin{aligned}
    \sum\vec{F_{ext}}=\frac{d\vec{p}}{dt}
\end{aligned}
```

La masse étant constante durant l'intégralité du mouvement on a :

```math
\begin{aligned}
        \sum\vec{F_{ext}}&= m\vec{a}\\
        \vec{P} + \vec{f_f} + \vec{\Pi_A} &= m\vec{a}\\
        m \vec{g} -\lambda \vec{v} - \rho_{air} V \vec{g} &= m\vec{a}\\
        -mg\vec{e_y} -\lambda (\vec{v_x}+\vec{v_y}) + \rho_{air} V g \vec{e_y} &= m(\vec{a_x}+\vec{a_y})\\
        -mg\vec{e_y} -\lambda\vec{v_x} -\lambda\vec{v_y} + \rho_{air} V g \vec{e_y} &= m\vec{a_x} + m\vec{a_y}\\
        -mg\vec{e_y} -\lambda\dot{x}\vec{e_x} -\lambda\dot{y}\vec{e_y} + \rho_{air} V g \vec{e_y} &= m\ddot{x}\vec{e_x} + m\ddot{y}\vec{e_y}\\
\end{aligned}
```

On obtient alors les composantes sur $`x`$ et $`y`$ suivantes :

```math
\begin{cases}
    \space \vec{e_x}/ \space \ddot{x} + \frac{\lambda}{m}\dot{x} = 0 \\
    \space \vec{e_y}/ \space \ddot{y} + \frac{\lambda}{m}\dot{y} = g(\rho_{air} V m^{-1} - 1)
\end{cases}
```

```math
\Downarrow
```

```math
\begin{aligned}
    \dot{x}(t) &= Ae^{-\frac{t}{\tau}}\\
    \dot{y}(t) &= Be^{-\frac{t}{\tau}} + \tau g(\rho_{air} V m^{-1} - 1)
\end{aligned}
```

On trouve les constante $`A`$ et $`B`$ grâce aux conditions initiales :

```math
\begin{aligned}
    &\begin{cases}
        \dot{x}(0) = v_0 \cos \alpha\\
        \dot{x}(0) = A
    \end{cases}\\
    &\begin{cases}
        \dot{y}(0) = v_0 \sin \alpha\\
        \dot{y}(0) = B + \tau g(\rho_{air} V m^{-1} - 1)
    \end{cases}
\end{aligned}
```

```math
\begin{aligned}
    &\implies A = v_0 \cos \alpha\\
    &\implies B = v_0 \sin (\alpha) + \tau g(1 - \rho_{air} V m^{-1})
\end{aligned}
```

```math
\begin{aligned}
    \dot{x}(t) &= v_0 \cos(\alpha) e^{-\frac{t}{\tau}}\\
    \dot{y}(t) &= (v_0 \sin (\alpha) + \tau g(1 - \rho_{air} V m^{-1})) e^{-\frac{t}{\tau}} + \tau g(\rho_{air} V m^{-1} - 1)
\end{aligned}
```

Puis en intégrant :

```math
\begin{aligned}
    x &= -\tau v_0 \cos (\alpha) e^{-\frac{t}{\tau}} + A'\\
    y &= -\tau(v_0 \sin (\alpha) + \tau g(1 - \rho_{air} V m^{-1})) e^{-\frac{t}{\tau}}  + \tau g(\rho_{air} V m^{-1} - 1) t + B'
\end{aligned}
```

On trouve les constante $`A'`$ et $`B'`$ grâce aux conditions initiales :

```math
\begin{aligned}
    &\begin{cases}
        x(0) = 0\\
        x(0) = -\tau v_0 \cos (\alpha) + A'
    \end{cases}\\
    &\begin{cases}
        y(0) = 0.3206\\
        y(0) = -\tau(v_0 \sin (\alpha) + \tau g(1 - \rho_{air} V m^{-1})) + B'
    \end{cases}
\end{aligned}
```

```math
\begin{aligned}
    &\implies A' = \tau v_0 \cos (\alpha)\\
    &\implies B' = 0.3206 + \tau(v_0 \sin (\alpha) + \tau g(1 - \rho_{air} V m^{-1}))
\end{aligned}
```

```math
\begin{aligned}
    x &= \tau v_0 \cos (\alpha) (1 - e^{-\frac{t}{\tau}})\\
    y &= \tau(v_0 \sin (\alpha) + \tau g(1 - \rho_{air} V m^{-1})) (1 - e^{-\frac{t}{\tau}}) + \tau g(\rho_{air} V m^{-1} - 1) t + 0.3206
\end{aligned}
```

Avec l'aide de l'outil en ligne Desmos, nous remarquons que l'influance de la force négligée, la poussée d'Archimède, est minime et invisible à l'oeil nu. La différence est de l'ordre du dixième de millimètre.

[Ici le graphique](https://www.desmos.com/calculator/fg4jr75gy5).

[schema-situation]: ./images/schema-situation.png


## Partie III - Prévoir

### Choisir une nouvelle situation expérimentale

Les seules variables pouvant être changés sont la bille, le poids du marteau et la distance $`H`$ entre la bille et l'axe $`A`$ autour duquel le marteau tourne.

Par manque de flexibilité, seule la distance $`H`$ va être modifiée et on choisira $`H = 55 \space cm = 55 \cdot 10^{-2} \space m`$.

Nous utiliserons, par ailleurs, l'équation parabolique du mouvement au vu de l'influance faible des force de frottements et poussée d'Archimède, soit: 

```math
\begin{aligned}
    x &= v_0 \cos (\alpha) t\\
    y &= - \frac{g}{2} t^2 + v_0 \sin (\alpha) t + 0.3206
\end{aligned}
```

On rappelle les constantes : 

```math
\begin{aligned}
    &g &&= 9.81 \space m.s^{-2} \\
    &\alpha &&= 43 \space °\\
    &m_b &&= 32.96 \cdot 10^{-3} \space kg\\
    &m_m &&= 721 \cdot 10^{-3} \space kg\\
    &v_0 &&= \frac{m_m}{m_b} \cdot \sqrt{2gH} \cdot \left( \sqrt{\cos(\alpha)} - \sqrt{\cos(\alpha) - 0.083} \right)
\end{aligned}
```

### Calculer la trajectoire grâce au modèle mathématique

Par application mathématique nous obtenons :

```math
\begin{aligned}
        x &= \left( v_0 \cos (\alpha) \approx 2.627 \right) t\\
        x &= 2.627 \cdot t
\end{aligned}
```

```math
\begin{aligned}
        y &= - \left( \frac{g}{2} = 4.905 \right) t^2 + \left( v_0 \sin (\alpha) \approx 2.45 \right) t + 0.3206\\
        y &= - 4.905 \cdot t^2 + 2.45 \cdot t + 0.3206
\end{aligned}
```

![graphique-prevision](./images/graphique-prevision.png)

[Ici le graphique](https://www.desmos.com/calculator/sabey9hsgd)

### Réaliser une nouvelle expérience

![pointage-55cm](./images/pointage-55cm.png)

[Ici la vidéo](./videos/2021-04-02 09-48-19_55cm.mkv)

### Confronter la prévision réalisée grâce au modèle, et la réalité de l'expérience

![graphique-prevision-points](./images/graphique-prevision-points.png)

Le modèle ne colle pas du tout au pointage, cependant nous remarquons que l'allure est très similaire. Nous confirmons cela en prenant $`H=70 \space cm`$ :

![theorique-70cm-pointage-55cm](./images/theorique-70cm-pointage-55cm.png)

L'erreur ne peut pas venir du pointage vidéo, la marge d'erreur est trop grande pour que ce soit une simple erreur de pointage.

L'erreur viendrait peut être de l'expérience, les inconnues autour du marteau pourraient influer sur le pointage, le fait que l'axe soit fait de corde et contienne le manche du marteau, les imperfections de la tête du marteau.

L'erreur parait cependant trop grande, l'erreur viendrait peut etre aussi du premier pointage sur lequel nous nous sommes basés sur la partie 1.

Toutes ces incertitudes auraient contribué à l'erreur.

L'approximation, cependant, est très juste (si l'on omet l'erreur), nous avons une approximation de l'ordre du centimètre.

Si nous avions eu recours à un pendule rigide, notre modèle aurait eu moins d'erreurs.

## Partie IV - Conclure

*Cf. Vidéo individuelle.*

## Partie V - Analyse reflexive

*Cf. Analyse reflexive individuelle.*